import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  searchNotfound = true
  searchLimit = 10
  queryPage = 1
  countList = 0
  searchOffset=0
  datas = [{name:'WAD TEAM'}]
  showNumber = [10, 25, 50, 100]
  constructor() { }

  ngOnInit() {
    this.countList = this.datas.length
    if(this.datas.length!=0) this.searchNotfound = false
  }
  onChangePage() {
    this.searchOffset = (this.searchLimit * this.queryPage) - this.searchLimit;
  }

}
